package ru.t1.vlvov.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.vlvov.tm.api.repository.dto.ISessionDtoRepository;
import ru.t1.vlvov.tm.api.repository.dto.IUserDtoRepository;
import ru.t1.vlvov.tm.api.repository.model.IUserRepository;
import ru.t1.vlvov.tm.api.service.IConnectionService;
import ru.t1.vlvov.tm.api.service.IPropertyService;
import ru.t1.vlvov.tm.dto.model.SessionDTO;
import ru.t1.vlvov.tm.marker.UnitCategory;
import ru.t1.vlvov.tm.repository.dto.SessionDtoRepository;
import ru.t1.vlvov.tm.repository.dto.UserDtoRepository;
import ru.t1.vlvov.tm.repository.model.UserRepository;
import ru.t1.vlvov.tm.service.ConnectionService;
import ru.t1.vlvov.tm.service.PropertyService;

import javax.persistence.EntityManager;

import static ru.t1.vlvov.tm.constant.SessionTestData.*;
import static ru.t1.vlvov.tm.constant.UserTestData.*;

@Category(UnitCategory.class)
public final class SessionRepositoryTest {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);

    private EntityManager getEntityManager() {
        return connectionService.getEntityManager();
    }

    @NotNull
    private ISessionDtoRepository getRepository(@NotNull final EntityManager entityManager) {
        return new SessionDtoRepository(entityManager);
    }

    @Before
    @SneakyThrows
    public void init() {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IUserDtoRepository repository = new UserDtoRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.add(USER1);
            repository.add(USER2);
            repository.add(ADMIN1);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @After
    @SneakyThrows
    public void tearDown() {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IUserRepository repository = new UserRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.removeById(USER1.getId());
            repository.removeById(USER2.getId());
            repository.removeById(ADMIN1.getId());
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }


    @Test
    @SneakyThrows
    public void add() {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final ISessionDtoRepository repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            System.out.println(repository);
            Assert.assertNull(repository.findOneById(USER1_SESSION1.getId()));
            System.out.println(USER1_SESSION1);
            repository.add(USER1_SESSION1);
            entityManager.getTransaction().commit();
            Assert.assertEquals(USER1_SESSION1.getId(), repository.findOneById(USER1_SESSION1.getId()).getId());
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    @SneakyThrows
    public void addByUserId() {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final ISessionDtoRepository repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.add(USER1_SESSION1);
            Assert.assertEquals(USER1.getId(), repository.findAll().get(0).getUserId());
            Assert.assertEquals(USER1_SESSION1.getId(), repository.findAll().get(0).getId());
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    @SneakyThrows
    public void findAllByUserId() {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final ISessionDtoRepository repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            for (final SessionDTO session : SESSION_LIST) {
                repository.add(session);
            }
            Assert.assertFalse(repository.findAll(USER1.getId()).isEmpty());
            repository.clear(USER1.getId());
            Assert.assertTrue(repository.findAll(USER1.getId()).isEmpty());
            Assert.assertFalse(repository.findAll().isEmpty());
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    @SneakyThrows
    public void findOneByIdByUserId() {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final ISessionDtoRepository repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.add(USER1_SESSION1);
            repository.add(USER2_SESSION1);
            Assert.assertEquals(USER1_SESSION1.getId(), repository.findOneById(USER1.getId(), USER1_SESSION1.getId()).getId());
            Assert.assertNull(repository.findOneById(USER1.getId(), USER2_SESSION1.getId()));
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    @SneakyThrows
    public void clearByUserId() {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final ISessionDtoRepository repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.add(USER1_SESSION1);
            repository.add(USER2_SESSION1);
            repository.clear(USER1.getId());
            Assert.assertTrue(repository.findAll(USER1.getId()).isEmpty());
            Assert.assertEquals(USER2_SESSION1.getId(), repository.findOneById(USER2_SESSION1.getId()).getId());
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    @SneakyThrows
    public void removeByIdByUserId() {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final ISessionDtoRepository repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.add(USER1_SESSION1);
            repository.add(USER2_SESSION1);
            repository.removeById(USER1.getId(), USER1_SESSION1.getId());
            Assert.assertNull(repository.findOneById(USER1_SESSION1.getId()));
            Assert.assertEquals(USER2_SESSION1.getId(), repository.findOneById(USER2_SESSION1.getId()).getId());
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    @SneakyThrows
    public void existsByIdByUserId() {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final ISessionDtoRepository repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.add(USER1_SESSION1);
            entityManager.getTransaction().commit();
            Assert.assertNotNull(repository.findOneById(USER1_SESSION1.getId()));
            Assert.assertNull(repository.findOneById(USER2_SESSION1.getId()));
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

}
