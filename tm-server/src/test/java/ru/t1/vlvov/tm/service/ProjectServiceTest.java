package ru.t1.vlvov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.vlvov.tm.api.service.IConnectionService;
import ru.t1.vlvov.tm.api.service.IPropertyService;
import ru.t1.vlvov.tm.api.service.dto.IProjectDtoService;
import ru.t1.vlvov.tm.api.service.dto.IUserDtoService;
import ru.t1.vlvov.tm.api.service.model.IUserService;
import ru.t1.vlvov.tm.dto.model.ProjectDTO;
import ru.t1.vlvov.tm.enumerated.Status;
import ru.t1.vlvov.tm.marker.UnitCategory;
import ru.t1.vlvov.tm.service.dto.ProjectDtoService;
import ru.t1.vlvov.tm.service.dto.UserDtoService;
import ru.t1.vlvov.tm.service.model.UserService;

import static ru.t1.vlvov.tm.constant.ProjectTestData.*;
import static ru.t1.vlvov.tm.constant.UserTestData.*;

@Category(UnitCategory.class)
public final class ProjectServiceTest {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private final IProjectDtoService projectService = new ProjectDtoService(connectionService);

    @NotNull
    private final IUserDtoService userServiceDTO = new UserDtoService(connectionService, propertyService);

    @NotNull
    private final IUserService userService = new UserService(connectionService, propertyService);

    @Before
    public void init() {
        userServiceDTO.add(USER1);
        userServiceDTO.add(USER2);
        userServiceDTO.add(ADMIN1);
    }

    @After
    public void tearDown() {
        userService.removeById(USER1.getId());
        userService.removeById(USER2.getId());
        userService.removeById(ADMIN1.getId());
    }

    @Test
    public void add() {
        Assert.assertTrue(projectService.findAll().isEmpty());
        projectService.add(USER1_PROJECT1);
        Assert.assertEquals(USER1_PROJECT1.getId(), projectService.findAll().get(0).getId());
    }

    @Test
    public void addByUserId() {
        Assert.assertTrue(projectService.findAll().isEmpty());
        projectService.add(USER1.getId(), USER1_PROJECT1);
        Assert.assertEquals(USER1_PROJECT1.getId(), projectService.findAll().get(0).getId());
        Assert.assertEquals(USER1.getId(), projectService.findAll().get(0).getUserId());
    }

    @Test
    public void clearByUserId() {
        Assert.assertTrue(projectService.findAll().isEmpty());
        for (final ProjectDTO project : USER1_PROJECT_LIST) projectService.add(project);
        Assert.assertFalse(projectService.findAll(USER1.getId()).isEmpty());
        projectService.clear(USER2.getId());
        Assert.assertFalse(projectService.findAll(USER1.getId()).isEmpty());
        projectService.clear(USER1.getId());
        Assert.assertTrue(projectService.findAll().isEmpty());
        projectService.add(USER2_PROJECT1);
        projectService.clear(USER1.getId());
        Assert.assertFalse(projectService.findAll().isEmpty());
    }

    @Test
    public void findAllByUserId() {
        Assert.assertTrue(projectService.findAll().isEmpty());
        for (final ProjectDTO project : PROJECT_LIST) projectService.add(project);
        Assert.assertFalse(projectService.findAll(USER1.getId()).isEmpty());
    }

    @Test
    public void findOneByIdByUserId() {
        projectService.add(USER1_PROJECT1);
        projectService.add(USER2_PROJECT1);
        Assert.assertEquals(USER1_PROJECT1.getId(), projectService.findOneById(USER1.getId(), USER1_PROJECT1.getId()).getId());
        Assert.assertNull(projectService.findOneById(USER1.getId(), USER2_PROJECT1.getId()));
    }

    @Test
    public void removeByUserId() {
        Assert.assertTrue(projectService.findAll().isEmpty());
        projectService.add(USER1_PROJECT1);
        projectService.add(USER2_PROJECT1);
        projectService.remove(USER1.getId(), USER1_PROJECT1);
        Assert.assertNull(projectService.findOneById(USER1_PROJECT1.getId()));
        Assert.assertFalse(projectService.findAll().contains(USER1_PROJECT1));
        Assert.assertNotNull(projectService.findOneById(USER2_PROJECT1.getId()));
    }

    @Test
    public void removeByIdByUserId() {
        Assert.assertTrue(projectService.findAll().isEmpty());
        projectService.add(USER1_PROJECT1);
        projectService.add(USER2_PROJECT1);
        projectService.removeById(USER1.getId(), USER1_PROJECT1.getId());
        Assert.assertNull(projectService.findOneById(USER1_PROJECT1.getId()));
        Assert.assertEquals(USER2_PROJECT1.getId(), projectService.findOneById(USER2_PROJECT1.getId()).getId());
    }

    @Test
    public void existsByIdByUserId() {
        Assert.assertTrue(projectService.findAll().isEmpty());
        projectService.add(USER1_PROJECT1);
        Assert.assertTrue(projectService.existsById(USER1_PROJECT1.getId()));
        Assert.assertFalse(projectService.existsById(USER2_PROJECT1.getId()));
    }

    @Test
    public void changeProjectStatusById() {
        projectService.add(USER1_PROJECT1);
        Assert.assertEquals(Status.NOT_STARTED, USER1_PROJECT1.getStatus());
        projectService.changeProjectStatusById(USER1_PROJECT1.getUserId(), USER1_PROJECT1.getId(), Status.IN_PROGRESS);
        Assert.assertEquals(Status.IN_PROGRESS, projectService.findOneById(USER1_PROJECT1.getId()).getStatus());
    }

    @Test
    public void createProjectName() {
        Assert.assertTrue(projectService.findAll().isEmpty());
        @NotNull ProjectDTO project = projectService.create(USER1.getId(), "test_project");
        Assert.assertEquals(project.getId(), projectService.findOneById(project.getId()).getId());
        Assert.assertEquals("test_project", project.getName());
        Assert.assertEquals(USER1.getId(), project.getUserId());
    }

    @Test
    public void createProjectNameDescription() {
        Assert.assertTrue(projectService.findAll().isEmpty());
        @NotNull ProjectDTO project = projectService.create(USER1.getId(), "test_project", "test_description");
        Assert.assertEquals(project.getId(), projectService.findOneById(project.getId()).getId());
        Assert.assertEquals("test_project", project.getName());
        Assert.assertEquals("test_description", project.getDescription());
        Assert.assertEquals(USER1.getId(), project.getUserId());
    }

    @Test
    public void updateById() {
        Assert.assertTrue(projectService.findAll().isEmpty());
        @NotNull ProjectDTO project = projectService.create(USER1.getId(), "test_project", "test_description");
        projectService.updateById(USER1.getId(), project.getId(), "new name", "new description");
        Assert.assertEquals("new name", projectService.findOneById(project.getId()).getName());
        Assert.assertEquals("new description", projectService.findOneById(project.getId()).getDescription());
    }

}
