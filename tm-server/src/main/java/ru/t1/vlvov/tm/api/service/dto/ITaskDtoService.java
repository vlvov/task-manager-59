package ru.t1.vlvov.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.vlvov.tm.dto.model.TaskDTO;
import ru.t1.vlvov.tm.enumerated.Status;

import java.util.List;

public interface ITaskDtoService extends IUserOwnedDtoService<TaskDTO> {

    @Nullable
    List<TaskDTO> findAllByProjectId(@NotNull String projectId);

    @Nullable
    List<TaskDTO> findAllByProjectId(@NotNull String userId, String projectId);

    @Nullable TaskDTO changeTaskStatusById(@Nullable String userId, @Nullable String id, @NotNull Status status);

    @Nullable TaskDTO updateById(@Nullable String userId, @Nullable String id, @Nullable String name, @NotNull String description);

    @Nullable
    TaskDTO create(@Nullable String userId, @Nullable String name);

    @Nullable
    TaskDTO create(
            @Nullable String userId,
            @Nullable String name,
            @NotNull String description
    );

}
