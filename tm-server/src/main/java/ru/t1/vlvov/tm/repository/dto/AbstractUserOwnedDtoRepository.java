package ru.t1.vlvov.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.vlvov.tm.api.repository.dto.IUserOwnedDtoRepository;
import ru.t1.vlvov.tm.dto.model.AbstractUserOwnedModelDTO;

@Repository
@Scope("prototype")
public abstract class AbstractUserOwnedDtoRepository<M extends AbstractUserOwnedModelDTO> extends AbstractDtoRepository<M> implements IUserOwnedDtoRepository<M> {

    @Override
    public void add(@NotNull final String userId, @NotNull final M model) {
        if (userId.isEmpty()) return;
        entityManager.persist(model);
    }

    @Override
    public void update(@NotNull final String userId, @NotNull final M model) {
        if (userId.isEmpty()) return;
        entityManager.merge(model);
    }

    @Override
    public void remove(@NotNull final String userId, @NotNull final M model) {
        if (userId.isEmpty()) return;
        entityManager.remove(model);
    }

}
