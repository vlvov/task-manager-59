package ru.t1.vlvov.tm.service.dto;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.vlvov.tm.api.repository.dto.IDtoRepository;
import ru.t1.vlvov.tm.api.service.dto.IDtoService;
import ru.t1.vlvov.tm.dto.model.AbstractModelDTO;
import ru.t1.vlvov.tm.exception.entity.EntityNotFoundException;
import ru.t1.vlvov.tm.exception.field.IdEmptyException;
import javax.persistence.EntityManager;
import java.util.Collection;
import java.util.List;

@Service
public abstract class AbstractDtoService<M extends AbstractModelDTO, R extends IDtoRepository<M>> implements IDtoService<M> {

    @NotNull
    @Getter
    //@Autowired
    protected EntityManager entityManager;


    @NotNull
    @Autowired
    protected ApplicationContext context;

    @NotNull
    @Autowired
    protected IDtoRepository<M> repositoryDTO;

    @Override
    @Transactional
    public void clear() {
        repositoryDTO.clear();
    }

    @Override
    @Transactional
    public void add(@Nullable M model) {
        if (model == null) throw new EntityNotFoundException();
        repositoryDTO.add(model);
    }

    @Override
    @Transactional
    public void update(@Nullable M model) {
        if (model == null) throw new EntityNotFoundException();
        repositoryDTO.update(model);
    }

    @Override
    @Transactional
    public void set(@NotNull Collection<M> collection) {
        repositoryDTO.set(collection);
    }

    @Override
    @Nullable
    public List<M> findAll() {
        return repositoryDTO.findAll();
    }

    @Override
    @Nullable
    public M findOneById(@Nullable String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repositoryDTO.findOneById(id);
    }

    @Override
    @Transactional
    public void remove(@Nullable M model) {
        if (model == null) throw new EntityNotFoundException();
        repositoryDTO.remove(model);
    }

    @Override
    @Transactional
    public void removeById(@Nullable String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        M model = findOneById(id);
        remove(model);
    }

    @Override
    @Transactional
    public boolean existsById(@Nullable String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return findOneById(id) != null;
    }

}
