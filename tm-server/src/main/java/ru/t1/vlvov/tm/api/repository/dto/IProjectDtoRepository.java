package ru.t1.vlvov.tm.api.repository.dto;

import ru.t1.vlvov.tm.dto.model.ProjectDTO;

public interface IProjectDtoRepository extends IUserOwnedDtoRepository<ProjectDTO> {
}
