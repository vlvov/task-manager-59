package ru.t1.vlvov.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.vlvov.tm.api.service.dto.IProjectDtoService;
import ru.t1.vlvov.tm.api.service.dto.IProjectTaskDtoService;
import ru.t1.vlvov.tm.api.service.dto.ITaskDtoService;
import ru.t1.vlvov.tm.dto.model.ProjectDTO;
import ru.t1.vlvov.tm.dto.model.TaskDTO;
import ru.t1.vlvov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.vlvov.tm.exception.entity.TaskNotFoundException;
import ru.t1.vlvov.tm.exception.field.IdEmptyException;

import java.util.List;

@Service
public final class ProjectTaskDtoService implements IProjectTaskDtoService {

    @NotNull
    @Autowired
    private ITaskDtoService taskDtoService;

    @NotNull
    @Autowired
    private IProjectDtoService projectServiceDTO;

    @Override
    @Transactional
    public void bindTaskToProject(@Nullable String userId, @Nullable String projectId, @Nullable String taskId) {
        if (userId == null || userId.isEmpty()) throw new IdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new IdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new IdEmptyException();
        if (!projectServiceDTO.existsById(userId, projectId)) throw new ProjectNotFoundException();
        TaskDTO task = taskDtoService.findOneById(userId, taskId);
        if (task == null) throw new TaskNotFoundException();
        task.setProjectId(projectId);
        taskDtoService.update(task);
    }

    @Override
    @Transactional
    public void removeProjectById(@Nullable String userId, @Nullable String projectId) {
        if (userId == null || userId.isEmpty()) throw new IdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new IdEmptyException();
        @Nullable final ProjectDTO projectDTO = projectServiceDTO.findOneById(userId, projectId);
        if (projectDTO == null) throw new ProjectNotFoundException();
        List<TaskDTO> tasks = taskDtoService.findAllByProjectId(projectId);
        if (tasks != null)
            for (final TaskDTO task : tasks) {
                taskDtoService.remove(task);
            }
        projectServiceDTO.remove(projectDTO);
    }

    @Override
    @Transactional
    public void unbindTaskFromProject(@Nullable String userId, @Nullable String projectId, @Nullable String taskId) {
        if (userId == null || userId.isEmpty()) throw new IdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new IdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new IdEmptyException();
        if (!projectServiceDTO.existsById(userId, projectId)) throw new ProjectNotFoundException();
        TaskDTO task = taskDtoService.findOneById(userId, taskId);
        if (task == null) throw new TaskNotFoundException();
        @Nullable final String taskProjectId = task.getProjectId();
        if (taskProjectId == null || taskProjectId.isEmpty() || !taskProjectId.equals(projectId))
            throw new TaskNotFoundException();
        task.setProjectId(null);
        taskDtoService.update(task);
    }

}
