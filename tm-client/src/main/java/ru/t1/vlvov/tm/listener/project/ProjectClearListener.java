package ru.t1.vlvov.tm.listener.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.vlvov.tm.dto.request.ProjectClearRequest;
import ru.t1.vlvov.tm.event.ConsoleEvent;

@Component
public final class ProjectClearListener extends AbstractProjectListener {

    @NotNull
    private final String DESCRIPTION = "Remove all projects.";

    @NotNull
    private final String NAME = "project-clear";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    @EventListener(condition = "@projectClearListener.getName() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.out.println("[CLEAR PROJECTS]");
        @Nullable final ProjectClearRequest request = new ProjectClearRequest(getToken());
        projectEndpoint.clearProject(request);
    }

}
