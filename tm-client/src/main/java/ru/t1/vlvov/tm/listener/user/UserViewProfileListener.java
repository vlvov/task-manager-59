package ru.t1.vlvov.tm.listener.user;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.vlvov.tm.dto.request.UserProfileRequest;
import ru.t1.vlvov.tm.dto.response.UserProfileResponse;
import ru.t1.vlvov.tm.enumerated.Role;
import ru.t1.vlvov.tm.event.ConsoleEvent;

@Component
public final class UserViewProfileListener extends AbstractUserListener {

    @NotNull
    private final String NAME = "user-view-profile";

    @NotNull
    private final String DESCRIPTION = "Display user profile.";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

    @Override
    @EventListener(condition = "@userViewProfileListener.getName() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.out.println("[USER VIEW PROFILE]");
        @NotNull final UserProfileRequest request = new UserProfileRequest(getToken());
        @NotNull final UserProfileResponse response = userEndpoint.profile(request);
        showUser(response.getUser());
    }

}